///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"


char* bol (enum Fix fix) {
   if (fix = TRUE)
      return "TRUE";
   return "False";
}
char* gender (enum Gender g){
   if (g==FEMALE)
      return "FEMALE";
   return "TRUE";
}
char* breed (enum Breed b){
   if (b==MAIN_COON)
      return "Main Coon";
   if (b==MANX)
      return "Manx";
   if (b==SHORTHAIR)
      return "Shorthair";
   if (b==PERSIAN)
      return "persian";
   if (b==SPHYNX)
      return "Sphynx";
   return NULL;

}
/// Decode the enum Color into strings for printf()
 char* colorName (enum Color color) {

   // @todo Map the enum Color to a string
   if (color==BLACK){
      return "black";
   }
   if (color==WHITE){
      return "white";
   }
   if (color==RED){
      return "red";
   }
   if (color==BLUE){
      return "blue";
   }
   if (color==GREEN){
      return "green";
   }
   if (color==PINK){
      return "pink";
   }
   return NULL; // We should never get here
 };


