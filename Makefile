###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

CC = gcc
CFLAGS = -g
TARGET = animalfarm


$(TARGET): main.o cat.o animals.o
		$(CC) $(CFLAGS) -o $(TARGET) main.o cat.o animals.o



clean:
	rm -f *.o $(TARGET)
